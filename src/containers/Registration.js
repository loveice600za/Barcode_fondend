/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Picker,
  Alert,
} from 'react-native';
import { Button } from "react-native-elements";
import { Post} from "../helpers/CallApi";
import { server } from "../helpers/config";
import { NavigationActions } from "react-navigation";
import Spinner from "react-native-loading-spinner-overlay";
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      typeDepartment:'',
      data: "",
      params: "",
      user_id: "",
      username: "",
      password: "",
      fristname: "",
      lastname: "",
      email: "",
      phone_number: "",
      faculty: "",
      department: "",
      checkpassword: "",
    };
  }

  insertUser() {
   var { user_id } = this.state;
   var { password } = this.state;
   var { fristname } = this.state;
   var { lastname } = this.state;
   var { email } = this.state;
   var { phone_number } = this.state;
   var { faculty } = this.state;
   var { department } = this.state;
   const { navigation } = this.props;
   var params = {
     user_id: user_id,
     password:password,
     fristname:fristname,
     lastname:lastname,
     email:email,
     phone_number:phone_number,
     faculty:faculty,
     department:department,
   };
    this.setState({ visible: true });
   Post('register',params)
   .then(async (response) =>{
     this.setState({ visible: false });
     if (response.data.affectedRows === 1) {
        Alert.alert("สมัครสำเร็จ");
     this.props.navigation.goBack();
     }else {
          Alert.alert("รหัสประจำตัวผู้ใช้งานซ้ำ");
        }
   })
 }

 validatedata = () => {
   const { user_id, password,checkpassword,fristname,lastname,email,faculty,department } = this.state;
   if (user_id == "") {
     Alert.alert("คำเตือน", "กรุณาใส่รหัสประจำตัวผู้ใช้งาน");
   } else if (password == "") {
     Alert.alert("คำเตือน", "กรุณาใส่รหัสผ่าน");
   } else if (checkpassword != password) {
     Alert.alert("คำเตือน", "กรุณาใส่รหัสผ่านให้ตรงกัน");
   } else if (fristname == "") {
     Alert.alert("คำเตือน", "กรุณาใส่ชื่อ");
   } else if (lastname == "") {
    Alert.alert("คำเตือน", "กรุณาใส่นามสกุล");
  } else if (email == "") {
   Alert.alert("คำเตือน", "กรุณาใส่ที่อยู่อีเมล์");
 } else if (faculty == "") {
  Alert.alert("คำเตือน", "กรุณาใส่คณะ");
} else if (department == "") {
 Alert.alert("คำเตือน", "กรุณาใส่สาขา");
   } else {
     this.insertUser();
   }

 };


  render() {
    return (
      <ScrollView>
      <View style={styles.container}>
      <Spinner
          visible={this.state.visible}
          textContent={"Loading..."}
          textStyle={{ color: "#FFF" }}
        />

      <View style={{alignSelf:'stretch'}}>
      <Text style={styles.heder}>สมัครสมาชิก</Text>
      <TextInput style={styles.textinput} placeholderTextColor='#fff' placeholder="รหัสประจำตัว" underlineColorAndroid={"transparent"}
      onChangeText={user_id => this.setState({ user_id })}/>
      <TextInput style={styles.textinput} secureTextEntry={true} placeholderTextColor='#fff' placeholder="รหัสผ่าน" underlineColorAndroid={"transparent"}
      onChangeText={password => this.setState({ password })}/>
      <TextInput style={styles.textinput} secureTextEntry={true} placeholderTextColor='#fff' placeholder="ใส่รหัสผ่านอีกครั้ง" underlineColorAndroid={"transparent"}
      onChangeText={checkpassword => this.setState({ checkpassword })}/>
       <TextInput style={styles.textinput} placeholderTextColor='#fff' placeholder="ชื่อ" underlineColorAndroid={"transparent"}
       onChangeText={fristname => this.setState({ fristname })}/>
       <TextInput style={styles.textinput} placeholderTextColor='#fff' placeholder="นามสกุล" underlineColorAndroid={"transparent"}
       onChangeText={lastname => this.setState({ lastname })}/>
       <TextInput style={styles.textinput} placeholderTextColor='#fff' placeholder="ที่อยู่ อีเมล์" underlineColorAndroid={"transparent"}
       onChangeText={email => this.setState({ email })}/>
       <TextInput style={styles.textinput} placeholderTextColor='#fff' placeholder="เบอร์โทรศัพท์" underlineColorAndroid={"transparent"}
      onChangeText={phone_number => this.setState({ phone_number })}/>
      <View style={styles.picker}>
  <Picker
 selectedValue={this.state.faculty} style={{height:40,color:'#fff',}}
 onValueChange={(faculty) => this.setState({faculty})}>
      <Picker.Item
      label='คณะ' />
      <Picker.Item
      label='วิศวกรรมศาสตร์'  value='วิศวกรรมศาสตร์' />
      <Picker.Item
      label='อื่นๆ'  value='อื่นๆ' />
  </Picker>


  </View>
       <View style={styles.picker}>
   <Picker
  selectedValue={this.state.department} style={{height:40,color:'#fff',}}
  onValueChange={(department) => this.setState({department})}>
       <Picker.Item
       label='สาขา' />
       <Picker.Item
       label='CPE'  value='cpe' />
       <Picker.Item
       label='IE'  value='ie' />
       <Picker.Item
       label='อื่นๆ'  value='อื่นๆ' />
   </Picker>
   </View>
   <View style={{justifyContent:'center',alignItems:'center',marginTop:1}}>
   <TouchableOpacity  onPress={() => this.validatedata()}>
   <View style={{justifyContent:'flex-end',alignItems:'flex-end',marginRight:10,width:60}}>
   <Text style={{fontSize:14,color:"#ffffff",fontWeight: 'bold'}}>Sign UP</Text>
 </View>
 </TouchableOpacity>
 </View>

      </View>
      </View>
       </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#36485f',
    paddingLeft: 60,
    paddingRight: 60,
  },
  heder:{
   fontSize:24,
   color:'#fff',
   paddingBottom:10,
   marginBottom:40,
   borderColor:'#199187',
   borderBottomWidth:1,
   fontWeight: 'bold',
  },
  textinput:{
    alignSelf:'stretch',
    height:40,
    marginBottom:20,
    color:'#fff',
    borderBottomColor:'#f8f8f8',
    borderBottomWidth:1,
    fontSize:16,
  },
  picker:{
    alignSelf:'stretch',
    height:40,
    marginBottom:30,
    borderBottomColor:'#f8f8f8',
    borderBottomWidth:1,
 },
});
