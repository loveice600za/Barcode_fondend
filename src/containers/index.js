import Home from './Home';
import Detail from './Detail';
import Login from './Login';
import Registration from './Registration';
import Splash from './Splash';
import Main from './Main';
export {
  Home,
  Detail,
  Login,
  Registration,
  Splash,
  Main,
}
