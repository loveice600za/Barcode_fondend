/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  AsyncStorage,
  Alert,
  TouchableOpacity,
  Picker,
  TextInput,

} from 'react-native';
import navigation from "react-navigation";
import { Post } from '../../helpers/CallApi';
import { server , localeStoreKey} from "../../helpers/config";
import { Button,CheckBox  } from "react-native-elements";

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
 var CardID =  AsyncStorage.getItem('CardID')
type Props = {};
export default class App extends Component<Props> {

  constructor(props) {
    super(props);
   this.state ={
      card_id: "",
      activities: "",

   }
  }

 async insertAT() {
   var { activities } = this.state;
    var CardID = await AsyncStorage.getItem('CardID')
    console.log("Card_ID",CardID);
   const { navigation } = this.props;
   var params = {
     card_id: CardID,
     activities: activities,
   };
   Post('activities',params)
   .then(async (response) =>{
   })
}

validatedata = () => {
  const {crad_id,activities} = this.state; {
    if (activities == "") {
      Alert.alert("คำเตือน", "กรุณาเลือกกิจกรรม");
  }else {
    this.insertAT();
     Alert.alert("คำเตือน", "บันกกิจกรรมสำเร็จ");
  }
};
}

  render() {
    return (
      <View style={styles.container}>
      <View style={styles.picker}>
  <Picker
 selectedValue={this.state.activities} style={{height:40,color:'black',}}
 onValueChange={(activities) => this.setState({activities})}>
      <Picker.Item
      label='กิจกรรมคณะ/วิทยาลัย' />
      <Picker.Item
      label='โครงการส่งเสริมกิจกรรมพัฒนานักศึกษาตามแนวนโยบายสถานศึกษา 3D คณะวิศวกรรมศาสตร'  value='โครงการส่งเสริมกิจกรรมพัฒนานักศึกษาตามแนวนโยบายสถานศึกษา 3D คณะวิศวกรรมศาสตร์' />
      <Picker.Item
      label='กิจกรรมแสดงนิทรรศการ 12 สิงหา วิศวกรรมศาสตร์เทิดไท้องค์ราชินี'  value='กิจกรรมแสดงนิทรรศการ 12 สิงหา วิศวกรรมศาสตร์เทิดไท้องค์ราชินี์' />
      <Picker.Item
      label='โครงการอบรมปฐมนิเทศนักศึกษาใหม่ คณะวิศวกรรมศาสตร์'  value='โครงการอบรมปฐมนิเทศนักศึกษาใหม่ คณะวิศวกรรมศาสตร์' />
  </Picker>
 </View>
 <View style={{flexDirection:"row",borderColor:'black',borderWidth:1,margin:10}}>
  <TextInput
    onChangeText={activities => this.setState({activities})}  
    underlineColorAndroid="transparent"
    style={styles.input}
    placeholder="อื่นๆ"
  />
  </View>
      <View style={{justifyContent:'center',alignItems:'center',marginTop:1}}>
              <Button
                onPress={() => this.validatedata()}
                title="บันทึก"
                icon={{
                    name: "save",
                    type: "font-awesome",
                    size: 20,
                    color: "white",
                    fontWeight: "bold"
                  }}
                buttonStyle={styles.buttonContainer}
              />
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  picker:{
    alignSelf:'stretch',
    height:40,
    borderColor:"black",
    borderWidth: 0.5,
    backgroundColor: '#DCDCDC',
    marginTop: 20,
    margin: 5,
 },
  buttonContainer: {
   margin: 6,
   padding: 10,
   borderWidth: 0,
   borderColor: "#fff",
   borderRadius: 5,
   backgroundColor: "#191970",
   height: 10,
   alignSelf: "center",
   paddingVertical: 20
 },
 input: {
   alignSelf: "stretch",
   fontSize: 16,
   padding: 10,
   borderRadius: 5,
   backgroundColor: "rgba(255,255,255,1)",
   flex: 1,
   paddingVertical: 5,
 },
});
