/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  AsyncStorage,
  Alert
} from 'react-native';
import navigation from "react-navigation";
import { Get } from '../../helpers/CallApi';
import { server , localeStoreKey} from "../../helpers/config";

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.displayData();
       // var  { CardID }  = this.props.navigation.state.params.CardID;
       // console.log(JSON.stringify(CardID));
    // var AA = this.props.navigation.g;
    // console.log('***********' + AA);
   this.state ={
     dataST:[],
   }
  }


 async displayData() {

   var CardID = await AsyncStorage.getItem('CardID')
   console.log("*****************************",CardID);
   Get("student/"+CardID).then(async response=>{
    console.log('*******************',response);
     var {data} = await response;
    if (response.data.length <=0){
        await AsyncStorage.removeItem('CardID');
        await this.props.navigation.navigate("Home");
        Alert.alert('ไม่พบข้อมูล')
    }else {
      this.setState({dataST : response.data})
       console.log('******** dataST ***********',response.data.length);
    }
  }).catch(err=>{
    console.log(err)
  })
}

parseData() {
  var results = this.state.dataST.map((data, i) => {
     return (
         <View style={styles.container}>

         <View style={styles.imgae}>
         <Image
             source={require("./../../../imgae/student.png")}
             style={{ width: 120, height: 120, marginTop:20  }}
           />
         </View>
        <View style={{flexDirection:'row',marginTop:40}}>
        <View style={{height: 30}}>
        <Text style={{fontSize:16,color:"black",fontWeight: 'bold',margin:10}}>รหัสนักศึกษา:</Text>
        </View>
        <View style={{height: 30,flex:2}}>
        <Text style={{fontSize:16,color:"black",margin:10}}>{data.student_id}</Text>
        </View>
        </View>

        <View style={{flexDirection:'row'}}>
        <View style={styles.boxdata}>
        <Text style={{fontSize:16,color:"black",fontWeight: 'bold',margin:10}}>ชื่อ นามสกุล:</Text>
        </View>
        <View style={{height: 30,flex:2}}>
        <Text style={{fontSize:16,color:"black",margin:10}}>{data.student_name}</Text>
        </View>
        </View>

        <View style={{flexDirection:'row'}}>
        <View style={styles.boxdata}>
        <Text style={{fontSize:16,color:"black",fontWeight: 'bold',margin:10}}>วันเกิด:</Text>
        </View>
        <View style={{height: 30,flex:2}}>
        <Text style={{fontSize:16,color:"black",margin:10}}>{data.birthday}</Text>
        </View>
        </View>
        <View style={{flexDirection:'row'}}>
        <View style={styles.boxdata}>
        <Text style={{fontSize:16,color:"black",fontWeight: 'bold',margin:10}}>คณะ:</Text>
        </View>
        <View style={{height: 30,flex:2}}>
        <Text style={{fontSize:16,color:"black",margin:10}}>{data.feculty}</Text>
        </View>
        </View>
        <View style={{flexDirection:'row'}}>
        <View style={styles.boxdata}>
        <Text style={{fontSize:16,color:"black",fontWeight: 'bold',margin:10}}>สาขา:</Text>
        </View>
        <View style={{height: 30,flex:2}}>
        <Text style={{fontSize:16,color:"black",margin:10}}>{data.department}</Text>
        </View>
        </View>

        <View style={{flexDirection:'row'}}>
        <View style={styles.boxdata}>
        <Text style={{fontSize:16,color:"black",fontWeight: 'bold',margin:10}}>อาจารย์ที่ปรึกษา:</Text>
        </View>
        <View style={{height: 30,flex:2}}>
        <Text style={{fontSize:16,color:"black",margin:10}}>{data.advisor_name}</Text>
        </View>
        </View>

        </View>


      );
  });
   return results;
   console.log('55555555',results);
 }

    // try {
    //     let CardID = await AsyncStorage.getItem('CardID')
    //     console.log(CardID);
    // } catch (error) {
    //   Alert.alert(error);
    // }
  render() {
    return (
      <View style={styles.container}>


    {/*ชื่อ-นามสกุล*/}

    {/*วัน-เดือน-ปีเกิด*/}

    {/*คณะ*/}

    {/*สาขา*/}

    {/*อาจารย์ที่ปรึกษา*/}

        {this.parseData()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#F5FCFF',
  },
  imgae: {
    justifyContent:'flex-start',
    alignItems:'center',
    alignSelf:'center'
  },
  boxdata:{
    height: 30,
  },
  boxapi: {
    height: 30,
    flex:2
  },
});
