/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  AsyncStorage,
  Picker
} from 'react-native';
import navigation from "react-navigation";
import { Get } from '../../helpers/CallApi';
import { server , localeStoreKey} from "../../helpers/config";
import ClassscheduleTM from "./ClassscheduleTM";
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class ClassscheduleST extends Component<Props> {
  static navigationOptions = {
    drawerLabel: () => null
  };
  constructor(props, context) {
    super(props, context);
   this.state ={
      dataExam:[],
      uri_:'',
       data: "",
       terms_data:'',
       params: "",
   }
  }

 async componentWillMount() {
    try {
       console.log("llllllllllllllllllllll")
      const value = await AsyncStorage.getItem("@MySuperStore:key5");
      const CardID = await AsyncStorage.getItem('CardID');
      console.log("value" + value);
      console.log("*****************************",CardID);
      // this.setState({ uri_: value });
     // console.log("uri" + this.state.url_);
    } catch (error) {
      console.log(error);
       // return value;
    }
    this.displayData();

  }

  async displayData() {
    console.log("displayData");
    const Value = await AsyncStorage.getItem("@MySuperStore:key5");
     const CardID = await AsyncStorage.getItem('CardID');
     // var TermYear = AsyncStorage.getItem("@MySuperStore:key", data.term_year);
     // var Terms = TermYear.toString();
     //console.log("*****************************",CardID);
     Get("exam/"+CardID+"/"+Value).then(async response=>{
       console.log("*************class****************",response);
       var { data } = response;
 if (response.data) {
   this.setState({ dataExam: response.data });
 }
});
  }

  ExamData () {
     var results = this.state.dataExam.map((data, i) => {
       console.log("***********results***********",results);
       return (
           <View style={styles.boxdata}>

           <View style={{flexDirection:'row',}}>
           <View style={{height:30}}>
           <Text style={{fontSize:16,color:"black",fontWeight: 'bold',margin:10}}>{data.subject_id}:</Text>
           </View>
           <View style={{height: 30,flex:2}}>
           <Text style={{fontSize:16,color:"blue",margin:10}}>{data.subject_name}</Text>
           </View>
           </View>

           <View style={{flexDirection:'row',}}>
           <View style={{height:30}}>
           <Text style={{fontSize:16,color:"black",margin:5}}>กลุ่มที่:</Text>
           </View>
           <View style={{height: 30,flex:2}}>
           <Text style={{fontSize:16,color:"black",margin:5}}>{data.sec}</Text>
           </View>
           <View style={{height:30}}>
           <Text style={{fontSize:16,color:"black",margin:5}}>หน่วยกิต:</Text>
           </View>
           <View style={{height: 30,flex:2}}>
           <Text style={{fontSize:16,color:"black",margin:5}}>{data.subject_unit}</Text>
           </View>
           </View>


           <View style={{flexDirection:'row',}}>
           <View style={{height:30}}>
           <Text style={{fontSize:16,color:"black",margin:5}}></Text>
           </View>
           <View style={{height: 30,flex:2}}>
           <Text style={{fontSize:16,color:"blue",margin:5}}>Midterm</Text>
           </View>
           <View style={{height:30}}>
           <Text style={{fontSize:16,color:"black",margin:5}}>ห้องสอบ:</Text>
           </View>
           <View style={{height: 30,flex:2}}>
           <Text style={{fontSize:16,color:"green",margin:5,fontWeight: 'bold'}}>{data.mid_class}</Text>
           </View>
           </View>


           <View style={{flexDirection:'row',}}>
           <View style={{height:30}}>
           <Text style={{fontSize:16,color:"black",margin:5}}>วัน:</Text>
           </View>
           <View style={{height: 30}}>
           <Text style={{fontSize:16,color:"black",margin:5}}>{data.mid_date}</Text>
           </View>
           <View style={{height:30}}>
           <Text style={{fontSize:16,color:"black",margin:5}}>เวลา:</Text>
           </View>
           <View style={{height: 30}}>
           <Text style={{fontSize:16,color:"red",margin:5}}>{data.mid_time}</Text>
           </View>
           </View>


           <View style={{flexDirection:'row',}}>
           <View style={{height:30}}>
           <Text style={{fontSize:16,color:"black",margin:5}}></Text>
           </View>
           <View style={{height: 30,flex:2}}>
           <Text style={{fontSize:16,color:"blue",margin:5}}>Final</Text>
           </View>
           <View style={{height:30}}>
           <Text style={{fontSize:16,color:"black",margin:5}}>ห้องสอบ:</Text>
           </View>
           <View style={{height: 30,flex:2}}>
           <Text style={{fontSize:16,color:"green",margin:5,fontWeight: 'bold'}}>{data.fin_class}</Text>
           </View>
           </View>

           <View style={{flexDirection:'row',}}>
           <View style={{height:30}}>
           <Text style={{fontSize:16,color:"black",margin:5}}>วัน:</Text>
           </View>
           <View style={{height: 30}}>
           <Text style={{fontSize:16,color:"black",margin:5}}>{data.fin_date}</Text>
           </View>
           <View style={{height:30}}>
           <Text style={{fontSize:16,color:"black",margin:5}}>เวลา:</Text>
           </View>
           <View style={{height: 30}}>
           <Text style={{fontSize:16,color:"red",margin:5}}>{data.fin_time}</Text>
           </View>
           </View>


           </View>


        );
     });
     return results;
  }


  render() {
    return (
      <ScrollView>

      <View style={styles.container}>
       {this.ExamData()}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#F5FCFF',
  },
   boxdata: {
     height:200,
     backgroundColor:'#B0E0E6',
     alignSelf:'stretch',
     borderColor:'black',
     borderWidth: 1,
   },
   picker:{
     alignSelf:'stretch',
     height:40,
     marginBottom:30,
     borderBottomColor:'black',
     borderBottomWidth:1,
  },
});
