/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  AsyncStorage,
  Picker,
  TouchableOpacity,

} from 'react-native';
import navigation from "react-navigation";
import { Get } from '../../helpers/CallApi';
import { server , localeStoreKey} from "../../helpers/config";

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class ClassscheduleST extends Component<Props> {
  constructor(props, context) {
    super(props, context);
    this.displayData();
   this.state ={
      dataTerm:[],
      terms_data:"",
      params: "",
      Terms:''

   }
  }

   async displayData(data) {
     var CardID = await AsyncStorage.getItem('CardID')
     console.log("*****************************",CardID);
     Get("studyingtm/"+CardID).then(async response=>{
       console.log("*************class****************",response);
       var { data } = response;
 if (response.data) {
   this.setState({ dataTerm: response.data });
 }
});
  }
  async getTerm(data) {
   try {
     await AsyncStorage.setItem("@MySuperStore:key",data.term_year,);
     this.setState({terms_data: data.term_year})
   } catch (error) {
     console.log(error);
   }
    console.log("Term_data***********" +this.state.terms_data)
   this.props.navigation.navigate("ClassscheduleST", {displayData: data});
 }
  TermData () {
     var results = this.state.dataTerm.map((data, i) => {
       console.log("***********results***********",results);
       return (
         <View style={styles.boxdata}>
         <View style={{ flex: 1,height:60,backgroundColor:'#fff',marginTop:2,}}>
         <Text style={{fontSize:16,color:"#3366CC",fontWeight: 'bold',margin:10,alignItems:'center',alignSelf:'center',justifyContent:'center',}}>{data.term_year}</Text>
         </View>
         <View style={{ flex: 1,height:60,backgroundColor:'#fff',marginTop:2,}}>
          <TouchableOpacity onPress={() => this.getTerm(data)}>
           <Text style={{fontSize:16,color:"red",fontWeight: 'bold',margin:10,alignItems:'center',alignSelf:'center',justifyContent:'center',marginLeft:10,}}>'แสดงตารางเรียน'</Text>
           </TouchableOpacity>
         </View>
         </View>


        );
     });
     return results;
  }


  render() {
    return (
      <ScrollView>

      <View style={styles.container}>
       {this.TermData()}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#F5FCFF',
  },
  boxdata: {
    flexDirection: 'row',
    height:50,
    backgroundColor:'#336666',
    alignSelf:'stretch',
  },

   picker:{
     alignSelf:'stretch',
     height:50,
     marginBottom:30,
     borderBottomColor:'black',
     borderBottomWidth:1,
  },
});
