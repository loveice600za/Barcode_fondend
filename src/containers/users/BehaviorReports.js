/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  AsyncStorage,
  Alert,
  TouchableOpacity,
  TextInput

} from 'react-native';
import navigation from "react-navigation";
import { Post } from '../../helpers/CallApi';
import { server , localeStoreKey} from "../../helpers/config";
import { Button,CheckBox  } from "react-native-elements";

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
 var CardID =  AsyncStorage.getItem('CardID')
type Props = {};
export default class App extends Component<Props> {

  constructor(props) {
    super(props);
   this.state ={
      card_id: "",
      behavior:'',
      hairText:'',
      mustacheText:'',
      wearText:'',
      isHair: false,
      isMustache: false,
      isWear: false
   }
  }

  async insertBT() {

    var { behavior, hairText, mustacheText, wearText } = this.state;
     var CardID = await AsyncStorage.getItem('CardID')
     console.log("Card_ID",CardID);
    const { navigation } = this.props;
    var params = {
      card_id: CardID,
      behavior: behavior,
      behavior1: hairText,
      behavior2: mustacheText,
      behavior3: wearText,
    };
    Post('behavior',params)
    .then(async (response) =>{
      Alert.alert("คำเตือน", "บันทึกกพฤติกรรมสำเร็จ");
      this.setState({ hairText: ''})
      this.setState({ mustacheText: ''})
      this.setState({ wearText: ''})
      this.setState({ isHair: false })
      this.setState({ isMustache: false})
      this.setState({ isWear: false })
    })
 }

 validatedata = async () => {
   const {crad_id,behavior, isHair, isMustache, isWear } = this.state;
   {
     if (isHair === true || isMustache === true || isWear === true || behavior.length>0) {
       if(isHair === true){
         await this.setState({ hairText : 'ผมยาว'})
       }
       if(isMustache === true){
         await this.setState({ mustacheText : 'หนวดยาว'})
       }
       if(isWear === true){
         await this.setState({ wearText : 'แต่งกายผิดระเบียบ'})
       }
       await this.insertBT();
   }else {
      Alert.alert("คำเตือน", "กรุณาเลือกพฤติกรรม");
   }
 };
 }

  render() {
    return (
      <View style={styles.container}>
  <CheckBox
  title='ผมยาว'
  checked={this.state.isHair}
  onPress={() => this.setState({ isHair : !this.state.isHair})}
/>
<CheckBox
  center
  title='หนวดยาว'
  checked={this.state.isMustache}
  onPress={() => this.setState({ isMustache : !this.state.isMustache})}
/>
<CheckBox
  title='แต่งกายผิดระเบียบ'
  checked={this.state.isWear}
  onPress={() => this.setState({ isWear : !this.state.isWear})}
/>
      <View style={{flexDirection:"row",borderColor:'black',borderWidth:1,margin:10,}}>
       <TextInput
         onChangeText={behavior => this.setState({behavior})}
         underlineColorAndroid="transparent"
         style={styles.input}
         placeholder="อื่นๆ"
       />
       </View>
       <View style={{flexDirection:'row',alignSelf:'center'}}>
       <View style={{justifyContent:'center',alignItems:'center',marginTop:1}}>
               <Button
                 onPress={() => this.validatedata()}
                 title="บันทึก"
                 icon={{
                     name: "save",
                     type: "font-awesome",
                     size: 20,
                     color: "white",
                     fontWeight: "bold"
                   }}
                 buttonStyle={styles.buttonContainer}
               />
               <Text>{this.state.hairText}</Text>
               <Text>{this.state.mustacheText}</Text>
               <Text>{this.state.wearText}</Text>

       </View>
       </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#F5FCFF',
  },
  input: {
    alignSelf: "stretch",
    fontSize: 16,
    padding: 10,
    borderRadius: 5,
    backgroundColor: "rgba(255,255,255,1)",
    flex: 1,
    paddingVertical: 5,
  },
  buttonContainer: {
   margin: 6,
   padding: 10,
   borderWidth: 0,
   borderColor: "#fff",
   borderRadius: 5,
   backgroundColor: "#191970",
   height: 10,
   alignSelf: "center",
   paddingVertical: 20
 },
});
