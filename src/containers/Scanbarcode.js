import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  fontFamily,
  ScrollView,
  ImageBackground,
  AsyncStorage,
  Alert,
  ToastAndroid,
  Platform,
  Navigator,
  PermissionsAndroid,
  BackHandler
} from "react-native";
import { Ionicons, FontAwesome } from "react-native-vector-icons";
import { NavigationActions } from "react-navigation";
import { Button } from "react-native-elements";
import { Post } from "../helpers/CallApi";
import { server, localeStoreKey } from "../helpers/config";
import Spinner from "react-native-loading-spinner-overlay";
import BarcodeScanner from "react-native-barcode-scanner-google";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

type Props = {};
export default class Scanbarcode extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      txtinputusername: "",
      visible: false,
      user_id: ""
    };

    this.readBarcode = this.readBarcode.bind(this);
  }

  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures.",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the camera");
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  componentDidMount() {
    this.requestCameraPermission();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    return true;
  };

  async readBarcode(data) {
    //อ่านค่าที่ได้จากตรงนี้
    Alert.alert(`Barcode '${data}' `);
    AsyncStorage.setItem("CardID", data);
    await this.props.navigation.navigate("InformationST");
  }

  render() {
    const { user_id, password } = this.state;
    const isEnabled = user_id.length > 0 && password.length > 0;
    return (
      <View style={styles.container}>
        <BarcodeScanner
          style={{ flex: 1 }}
          onBarcodeRead={({ data, type }) => {
            // handle your scanned barcodes here!
            // as an example, we show an alert:
            //Alert.alert(`Barcode '${data}' of type '${type}' was scanned.`);
            this.readBarcode(data);
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: "stretch",
    backgroundColor: "black"
  },
  logo: {
    flexDirection: "row",
    height: 300,
    justifyContent: "flex-start",
    alignItems: "flex-start",
    alignSelf: "center"
  },
  input: {
    alignSelf: "stretch",
    fontSize: 16,
    padding: 10,
    marginBottom: 10,
    borderRadius: 5,
    backgroundColor: "rgba(255,255,255,1)",
    flex: 1,
    paddingVertical: 5
  },
  inputContainer: {
    marginBottom: 10,
    padding: 20,
    paddingBottom: 50,
    alignSelf: "stretch",
    marginTop: 40,
    backgroundColor: "rgba(255,255,255,0.2)",
    borderWidth: 1,
    borderColor: "#fff",
    margin: 10
  },
  buttonContainer: {
    flexDirection: "row",
    marginBottom: 0,
    padding: 2,
    borderWidth: 0,
    borderColor: "#fff",
    borderRadius: 5,
    backgroundColor: "#00FFFF",
    flex: 1,
    alignSelf: "stretch",
    paddingVertical: 20,
    marginLeft: 0,
    marginRight: 0
  },
  BoxContainer2: {
    flex: 10,
    margin: 0
  },
  BoxContainer4: {
    margin: 6,
    marginBottom: 0,
    padding: 0,
    paddingBottom: 10,
    alignSelf: "stretch",
    justifyContent: "center",
    flex: 3,
    height: 50
  },
  BoxContainer5: {
    margin: 6,
    marginBottom: 0,
    padding: 0,
    paddingBottom: 10,
    alignSelf: "stretch",
    justifyContent: "center",
    flex: 1
  }
});
