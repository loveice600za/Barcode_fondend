import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Picker,
  Alert,
  Image,
  AsyncStorage
} from "react-native";
import { Ionicons, FontAwesome } from "react-native-vector-icons";
import { Button } from "react-native-elements";
import { NavigationActions } from "react-navigation";
import { server, localeStoreKey } from "../helpers/config";
import { Get } from "../helpers/CallApi";
import Spinner from "react-native-loading-spinner-overlay";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});
console.disableYellowBox = true;
type Props = {};
export default class Home extends Component<Props> {
  componentDidMount() {
    this.clearData();
  }

  constructor(props) {
    super(props);
    this.checkLogin();
    this.state = {
      TextInputValue: "",
      CardID: "",
      visible: false
    };
  }

  buttonClicksendData = async () => {
    this.setState({ visible: true });
    const { TextInputValue } = this.state;
    await console.log(TextInputValue);
    await AsyncStorage.setItem("CardID", TextInputValue);
    var tesCard = await AsyncStorage.getItem("CardID");
    this.setState({ visible: false });
    await console.log("==== resulttesCard ======", tesCard);
    await console.log("555555", TextInputValue);
    await this.props.navigation.navigate("InformationST");
  };
  clearData() {
    AsyncStorage.removeItem("CardID");
  }

  async checkLogin() {
    console.log("==========checkLogin========");
    var token = await AsyncStorage.getItem(localeStoreKey.activeUser);
    if (token && token != null && token != "") {
      console.log("======token=====", token);
      return;
    }

    this.props.navigation.navigate("Login");
  }
  render() {
    return (
      <View style={styles.container}>
        <Spinner
          visible={this.state.visible}
          textContent={"Loading..."}
          textStyle={{ color: "#FFF" }}
        />
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("Scanbarcode")}
        >
          <View style={styles.imgae}>
            <Image
              source={require("../../imgae/qr-code.png")}
              style={{ width: 100, height: 100 }}
            />
          </View>
          <Text style={{ fontSize: 20, color: "white", marginBottom: 80 }}>
            'กดเพื่อสแกนบัตร'
          </Text>
        </TouchableOpacity>
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <TextInput
              underlineColorAndroid="transparent"
              style={styles.textinput}
              placeholder="กรอกเลขบัตรประชาชน......"
              onChangeText={TextInputValue => this.setState({ TextInputValue })}
            />
            <TouchableOpacity onPress={() => this.buttonClicksendData()}>
              <View style={{ alignSelf: "center" }}>
                <Image
                  source={require("../../icons/login.png")}
                  style={{ width: 30, height: 30, marginRight: 10 }}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <Button
          onPress={() => this.props.navigation.navigate("Login")}
          title="ออกจากระบบ"
          buttonStyle={styles.buttonContainer}
          icon={{
            name: "sign-out",
            type: "font-awesome",
            size: 20,
            color: "red",
            fontWeight: "bold",
            fontSize: 10
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "black"
  },
  imgae: {
    justifyContent: "flex-start",
    alignItems: "center",
    alignSelf: "center"
  },
  textinput: {
    alignSelf: "stretch",
    fontSize: 16,
    padding: 10,
    borderRadius: 5,
    backgroundColor: "rgba(255,255,255,1)",
    paddingVertical: 5,
    marginLeft: 30,
    marginRight: 10,
    flex: 1
  },
  buttonContainer: {
    flexDirection: "row",
    marginBottom: 0,
    padding: 2,
    borderWidth: 0,
    borderColor: "#fff",
    borderRadius: 5,
    backgroundColor: "black",
    alignSelf: "stretch",
    paddingVertical: 20,
    marginLeft: 0,
    marginRight: 0,
    height: 30,
    marginTop: 40
  }
});
