import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { middleware } from '../navigators/RootNavigator';
import rootReducer from '../reducers'

export default (initialState) => {
  const middlewares = [middleware, thunk]

  middlewares.push(createLogger())

  const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(...middlewares)
  )

  return store
}
