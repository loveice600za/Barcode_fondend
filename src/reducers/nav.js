import { NavigationActions } from 'react-navigation';
import { RootNavigator } from '../navigators/RootNavigator';

const HomeAction = RootNavigator.router.getActionForPathAndParams('Home');
const initialState = RootNavigator.router.getStateForAction(HomeAction);


const nav = (state = initialState, action) => {
  let nextState;
  switch (action.type) {
    case 'Login':
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.back(),
        state
      );
      break;
    case 'Logout':
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Login' }),
        state
      );
      break;
    case 'NAV_MAIN':
      nextState = TransNavigator.router.getStateForAction(action, state);
      break;
    case 'NAV/TO':
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: action.route }),
        state
      );
      break;
    default:
      nextState = RootNavigator.router.getStateForAction(action, state);
      break;
  }

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
};

export default nav;
