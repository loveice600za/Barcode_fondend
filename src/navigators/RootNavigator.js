import * as React from "react";
import { BackHandler, StyleSheet, ScrollView, View, Text } from "react-native";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  addNavigationHelpers,
  DrawerNavigator,
  StackNavigator,
  NavigationActions,
  DrawerItems,
  SafeAreaView,
  AsyncStorage
} from "react-navigation";
import {
  createReduxBoundAddListener,
  createReactNavigationReduxMiddleware
} from "react-navigation-redux-helpers";
import Icon from "react-native-vector-icons/Ionicons";
import { Home, Detail, Login, Registration, Main } from "../containers";

import {
  InformationST,
  ClassscheduleST,
  BehaviorReports,
  Activities,
  ClassscheduleTM,
  ExamTM,
  ExamST
} from "../containers/users";

import { localeStoreKey } from "../helpers/config";
import { Get } from "../helpers/CallApi";

import Scanbarcode from "../containers/Scanbarcode";

const USERNAME = "";

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

const DrawerIcon = ({ navigate }) => {
  return (
    <Icon
      name={"md-menu"}
      size={28}
      color={"#fff"}
      onPress={() => navigate("DrawerOpen")}
      style={{ paddingLeft: 20 }}
    />
  );
};

const CustomDrawerContentComponent = props => (
  <ScrollView>
    <SafeAreaView
      style={styles.container}
      forceInset={{ top: "always", horizontal: "never" }}
    >
      <View
        style={{
          backgroundColor: "#303030",
          paddingHorizontal: 15,
          paddingVertical: 15
        }}
      >
        <Text style={{ color: "#fff" }}>USER:{USERNAME}</Text>
      </View>
      <DrawerItems {...props} />
    </SafeAreaView>
  </ScrollView>
);

export const StackHome = StackNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      title: "หน้าแรก",
      header: false
    }
  },
  Scanbarcode: {
    screen: Scanbarcode,
    navigationOptions: {
      title: "หน้าแรก",
      header: false
    }
  }
});
export const StackMain = StackNavigator({
  Main: {
    screen: Main,
    navigationOptions: {
      header: false
    }
  }
});
export const StackLogin = StackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      title: "ออกจากระบบ",
      header: false
    }
  },
  Registration: {
    screen: Registration,
    navigationOptions: {
      header: false
    }
  }
});

export const StackDetail = StackNavigator({
  Detail: {
    screen: Detail,
    navigationOptions: ({ navigation }) => ({
      title: "Detail",
      headerLeft: <DrawerIcon {...navigation} />,
      headerStyle: {
        backgroundColor: "#212121"
      },
      headerTintColor: "#fff"
    })
  }
});
export const StackInformationST = StackNavigator({
  InformationST: {
    screen: InformationST,
    navigationOptions: ({ navigation }) => ({
      title: "ข้อมูลนักศึกษา",
      headerLeft: <DrawerIcon {...navigation} />,
      headerStyle: {
        backgroundColor: "#212121"
      },
      headerTintColor: "#fff"
    })
  }
});
export const StackClassscheduleST = StackNavigator({
  ClassscheduleST: {
    screen: ClassscheduleST,
    navigationOptions: ({ navigation }) => ({
      title: "ข้อมูลตารางเรียน",
      headerLeft: <DrawerIcon {...navigation} />,
      headerStyle: {
        backgroundColor: "#212121"
      },
      headerTintColor: "#fff"
    })
  }
});
export const StackClassscheduleTM = StackNavigator({
  ClassscheduleTM: {
    screen: ClassscheduleTM,
    navigationOptions: ({ navigation }) => ({
      title: "ข้อมูลตารางเรียน",
      headerLeft: <DrawerIcon {...navigation} />,
      headerStyle: {
        backgroundColor: "#212121"
      },
      headerTintColor: "#fff"
    })
  },
  ClassscheduleST: {
    screen: ClassscheduleST,
    navigationOptions: ({ navigation }) => ({
      title: "ข้อมูลตารางเรียน",
      headerLeft: <DrawerIcon {...navigation} />,
      headerStyle: {
        backgroundColor: "#212121"
      },
      headerTintColor: "#fff"
    })
  }
});

export const StackExamTM = StackNavigator({
  ExamTM: {
    screen: ExamTM,
    navigationOptions: ({ navigation }) => ({
      title: "ข้อมูลตารางสอบ",
      headerLeft: <DrawerIcon {...navigation} />,
      headerStyle: {
        backgroundColor: "#212121"
      },
      headerTintColor: "#fff"
    })
  },
  ExamST: {
    screen: ExamST,
    navigationOptions: ({ navigation }) => ({
      title: "ข้อมูลตารางสอบ",
      headerLeft: <DrawerIcon {...navigation} />,
      headerStyle: {
        backgroundColor: "#212121"
      },
      headerTintColor: "#fff"
    })
  }
});

export const StackBehaviorReports = StackNavigator({
  BehaviorReports: {
    screen: BehaviorReports,
    navigationOptions: ({ navigation }) => ({
      title: "บันทึกพฤติกรรมผิดระเบียบ",
      headerLeft: <DrawerIcon {...navigation} />,
      headerStyle: {
        backgroundColor: "#212121"
      },
      headerTintColor: "#fff"
    })
  }
});
export const StackActivities = StackNavigator({
  Activities: {
    screen: Activities,
    navigationOptions: ({ navigation }) => ({
      title: "บันทึกกิจกรรม",
      headerLeft: <DrawerIcon {...navigation} />,
      headerStyle: {
        backgroundColor: "#212121"
      },
      headerTintColor: "#fff"
    })
  }
});
export const StackExamST = StackNavigator({
  ExamST: {
    screen: ExamST,
    navigationOptions: ({ navigation }) => ({
      title: "ข้อมูลตารางสอบ",
      headerLeft: <DrawerIcon {...navigation} />,
      headerStyle: {
        backgroundColor: "#212121"
      },
      headerTintColor: "#fff"
    })
  }
});

export const RootNavigator = DrawerNavigator(
  {
    Home: {
      screen: StackHome
    },
    Detail: {
      screen: StackDetail
    },
    InformationST: {
      screen: StackInformationST
    },
    ClassscheduleTM: {
      screen: StackClassscheduleTM
    },
    ClassscheduleST: {
      screen: StackClassscheduleST
    },
    ExamTM: {
      screen: StackExamTM
    },
    ExamST: {
      screen: StackExamST
    },
    BehaviorReports: {
      screen: StackBehaviorReports
    },
    Activities: {
      screen: StackActivities
    },
    Login: {
      screen: StackLogin
    }
  },
  {
    navigationOptions: {},
    contentComponent: props => <CustomDrawerContentComponent {...props} />
  }
);

const mapStateToProps = state => ({
  nav: state.nav
});

export const middleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav
);

const addListener = createReduxBoundAddListener("root");

class RootWithNavigationState extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.shape({}).isRequired
  };
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
  }
  onBackPress = () => {
    const { dispatch, nav } = this.props;
    // if (nav.routes[0].routes[0].index === 0) {
    //   return false;
    // }
    dispatch(NavigationActions.back());
    return true;
  };
  componentWillMount() {
    this.getuse();
  }
  async getuse() {
    try {
      USERNAME = await AsyncStorage.getItem(localeStoreKey.activeUser)
      console.log("USERRRRR",USERNAME);
    } catch (error) {}
  }

  render() {
    const { dispatch, nav } = this.props;
    const navigation = addNavigationHelpers({
      dispatch,
      state: nav,
      addListener
    });

    return <RootNavigator navigation={navigation} />;
  }
}

export default connect(mapStateToProps)(RootWithNavigationState);
