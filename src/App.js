import React, { Component } from 'react';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import RootWithNavigationState from './navigators/RootNavigator';
import Splash from './containers/Splash';
 class App extends Component {
  store = configureStore({});

  render() {
    return (
      <Provider store={this.store}>
        <RootWithNavigationState />
      </Provider>
    );
  }
}
class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {currentScreen: 'Splash'};
    console.log('Start doing some tasks for about 3 seconds')
    setTimeout(()=>{
      console.log('Done some tasks for about 3 seconds')
      this.setState({currentScreen:'Login'})
    }, 1500)
  }
   render(){
     const { currentScreen } = this.state
     const mainScreen= currentScreen === 'Splash' ? <Splash /> : <App/>
     return mainScreen
   }
}
export default Main;
