import axios from 'axios';
import {server, localeStoreKey} from './config';
import {
  AsyncStorage
} from 'react-native';

export async function Post (path, params) {
  var url = server + path;
  console.log("========url=================>>>>",url );
  try {
  //   var token = await AsyncStorage.getItem(localeStoreKey.token);
  }
  catch (error) {
  //   console.log(error);
  }


  return axios.post(
    url,
    params,
  )

}

export async function PutFormData (path, params) {
  var url = server + path;
  try {
    var token = await AsyncStorage.getItem(localeStoreKey.token);
  } catch (error) {
    console.log(error);
  }

  header = HeaderToken(token);
  return axios({
    url,
    method: 'PUT',
    data: params,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
      "Authorization" : token
    }
  })
}

export  function Get (path, params) {
  var url = server + path;
  console.log("========url=================>>>>",url );
  //   return axios.get(url);
  // console.log("========url=================>>>>",url );

  try {
    // var token = await AsyncStorage.getItem(localeStoreKey.token);
  }
  catch (error) {

  }

  // header = HeaderToken(token);
  return axios.get(
    url,{}
    // { headers: {
    //   "Authorization" : token
    //   }
    // }
  )

}

export async function Put (path, params) {
  var url = server + path;
  try {
    var token = await AsyncStorage.getItem(localeStoreKey.token);
  }
  catch (error) {

  }

  header = HeaderToken(token);

  return axios.put(
    url,
    params,
    { headers: {
        "Authorization" : token
      }
    }
  )
}

export async function Delete (path, params) {
  var url = server + path;
  try {
    var token = await AsyncStorage.getItem(localeStoreKey.token);
  }
  catch (error) {

  }

  header = HeaderToken(token);

  return axios.delete(
    url,
    { headers: {
        "Authorization" : token
      }
    }
  )
}

export function HeaderToken(token) {
  if (token) {
    return {
      'Authorization': token
    };
  } else {
    return {};
  }
}
