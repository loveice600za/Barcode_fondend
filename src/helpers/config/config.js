import {
  Platform,
  AsyncStorage
} from 'react-native';

// let server = (Platform.OS == "ios" ? "http://192.168.3.101:3000/" : "http://192.168.3.101:3000/");
let server = (Platform.OS == "ios" ? "http://192.168.1.111:3000/" : "http://192.168.1.111:3000/");
// let server = (Platform.OS == "ios" ? "https://surroundme.co/api/" : "https://surroundme.co/api/");

let localeStoreKey = {
  activeUser: "@authactiveUser:activeUser",

};

export {server, localeStoreKey};
