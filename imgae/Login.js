/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  fontFamily,
  ScrollView,
  ImageBackground,
  AsyncStorage,
  Alert,
  ToastAndroid,
  Platform,
  Navigator
} from 'react-native';
import { Ionicons, FontAwesome } from "react-native-vector-icons";
import { NavigationActions } from "react-navigation";
import { Button } from "react-native-elements";


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
      super(props);
      this.state = {
        username: "",
        password: "",
        txtinputusername: "",
        visible: false
      };
    }


  render() {
    const { username, password } = this.state;
    const isEnabled = username.length > 0 && password.length > 0;
    return (
      <View style={styles.container}>
            <ImageBackground
              style={{justifyContent: "center",flex:1,}}
              source={require("../../imgae/RMUTT-1.jpg")}>

             <View style={{flex:1}}/>
             <Image
            style={styles.logo}
            source={require("../../imgae/RMUTT-logo.png")}
          />
           <View style={styles.inputContainer}>
           <View
           style={{
             flexDirection: "row",
             alignSelf: "stretch",
             flex: 0,
             alignItems: "stretch"
           }}>

           <Image
               source={require("../../icons/account.png")}
               style={{ width: 30, height: 30, margin: 2 }}
             />
           <Text> </Text>
           <TextInput
             underlineColorAndroid="transparent"
             style={styles.input}
             placeholder="Username"
             onChangeText={username => this.setState({ username })}
           />
           </View>

           <View
           style={{
             flexDirection: "row",
             alignSelf: "stretch",
             flex: 0,
             alignItems: "stretch"
           }}
           >
           <Image
               source={require("../../icons/Key.png")}
               style={{ width: 30, height: 30, margin: 2 }}
             />
           <Text> </Text>
           <TextInput
             secureTextEntry={true}
             underlineColorAndroid="transparent"
             style={styles.input}
             placeholder="Psssword"
             onChangeText={password => this.setState({ password })}
           />
           </View>

           <View
             style={{
               flexDirection: "row",
               alignSelf: "stretch",
               flex: 3,
               alignItems: "stretch",
             }}
           >
            <View style={styles.BoxContainer4} />
            <View style={styles.BoxContainer2}>
           <Button
                 onPress={() => this.login()}
                 disabled={!isEnabled}
                 onSubmit={this.handleSubmit}
                 title="Sign in"
                 buttonStyle={styles.buttonContainer}
                 icon={{
                   name: "sign-in",
                   type: "font-awesome",
                   size: 20,
                   color: "white",
                   fontWeight: "bold",
                   fontSize:10
                 }}
               />
               </View>
                <View style={styles.BoxContainer4} >
                <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Detail")}>
                <View style={{justifyContent:'flex-end',alignItems:'flex-end',marginRight:10,borderBottomWidth: 1,borderColor: "#ffffff",width:60}}>
                <Text style={{fontSize:14,color:"#ffffff",fontWeight: 'bold'}}>Register</Text>
              </View>
              </TouchableOpacity>
               </View>
               </View>

           </View>
          <View style={styles.BoxContainer5} />
              </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   alignSelf: "stretch",
   backgroundColor: "black",

 },
  logo: {
   flexDirection: "row",
   height: 210,

 },
 input: {
   alignSelf: "stretch",
   fontSize: 16,
   padding: 10,
   marginBottom: 10,
   borderRadius: 5,
   backgroundColor: "rgba(255,255,255,1)",
   flex: 1,
   paddingVertical: 5
 },
 inputContainer: {
   marginBottom: 10,
   padding: 20,
   paddingBottom: 50,
   alignSelf: "stretch",
   marginTop:40,
   backgroundColor:'rgba(255,255,255,0.2)',
   borderWidth:1,
   borderColor:'#fff',
   margin:10

 },
 buttonContainer: {
    flexDirection: "row",
    marginBottom: 0,
    padding: 2,
    borderWidth: 0,
    borderColor: "#fff",
    borderRadius: 5,
    backgroundColor: "#00FFFF",
    flex: 1,
    alignSelf: "stretch",
    paddingVertical: 20,
    marginLeft:0,
    marginRight:0,
  },
  BoxContainer2: {
   flex: 10,
   margin: 0,
 },
 BoxContainer4: {
   margin: 6,
   marginBottom: 0,
   padding: 0,
   paddingBottom: 10,
   alignSelf: "stretch",
   justifyContent: "center",
   flex: 3,
   height:50
 },
 BoxContainer5: {
   margin: 6,
   marginBottom: 0,
   padding: 0,
   paddingBottom: 10,
   alignSelf: "stretch",
   justifyContent: "center",
   flex: 1,

 }
});
